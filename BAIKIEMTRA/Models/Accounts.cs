﻿using System.ComponentModel.DataAnnotations;

namespace BAIKIEMTRA.Models
{
    public class Accounts
    {
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string AccountName { get; set; }

        public Reports? Reports { get; set; }

        public string AddTextHere { get; set; }

    }
}
