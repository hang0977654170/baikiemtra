﻿using System.ComponentModel.DataAnnotations;

namespace BAIKIEMTRA.Models
{
    public class Reports
    {
        public int Id { get; set; }
        [Required, StringLength(100)]
        public string AccoutId { get; set; }
        [Range(0.01, 10000.00)]
        public string LogId { get; set; }
        public string TransactionalId { get; set; }
        public string ReportName { get; set; }

        public string ReportDate { get; set; }
        public string AddTextHere { get; set; }
    }
} 
